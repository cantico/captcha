<?php
require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

if(!function_exists('array_chunk')) {
    function array_chunk( $input, $size, $preserve_keys = false) {
        @reset( $input );

        $i = $j = 0;

        while( @list( $key, $value ) = @each( $input ) ) {
            if( !( isset( $chunks[$i] ) ) ) {
               $chunks[$i] = array();
            }

            if( count( $chunks[$i] ) < $size ) {
                if( $preserve_keys ) {
                    $chunks[$i][$key] = $value;
                    $j++;
                } else {
                    $chunks[$i][] = $value;
                }
            } else {
                $i++;

                if( $preserve_keys ) {
                    $chunks[$i][$key] = $value;
                    $j++;
                } else {
                    $j = 0;
                    $chunks[$i][$j] = $value;
                }
            }
        }
        return $chunks;
    }
}

class Func_Captcha extends bab_functionality
{
	var $randString				= '';
	var $stringLength			= 10;
	var $imageWidth				= false;
	var $imageHeight			= false;
	var $background				= true;
	var $backgroundColor		= array('R' => 255, 'V' => 255, 'B' => 255);
	var $borderColor			= array('R' => 226, 'V' => 113, 'B' => 59);
	var $borderWidth			= 0;
	var $textColor				= array('R' => 50, 'V' => 50, 'B' => 50);
//	var $forbiddenChars			= array(1, 0, 'l', '0');
	var $font					= '';
	var $fontSize				= 15;
	var $fromBorder				= 10;
	var $type					= '';
	var $shadow					= false;
	var $shadowColor			= array('R' => 128, 'V' => 128, 'B' => 128);
	var $shadowX				= 2;
	var $shadowY				= 2;
	var $backgroundImage		= false;
	var $textAngle				= 0;
	var $roundedCorners			= false;
	var $roundedCornersRadius	= 5;

	function Func_Captcha()
	{
		parent::bab_functionality();


		require_once dirname(__FILE__) . '/traduction.php';
		require_once dirname(__FILE__) . '/defines.php';

		$this->setImageType('jpeg');

		$this->setFont(dirname(__FILE__) . '/fonts/Tuffy_Bold.ttf', 17);

		$oInfo = bab_getAddonInfosInstance('captcha');
		if ($oInfo) {
			$this->setBackgroundImage($oInfo->getImagesPath() . '/bg.jpg');
		}
	}

	function getDescription()
	{
		return captcha_translate("Captcha functionality");
	}

	/**
	 * Sets the length of the random string.
	 *
	 * @param int $lenght length in characters
	 */
	function setStringLenght($lenght)
	{
		$this->stringLength = $lenght;
	}

	/**
	 * Sets the background color.
	 *
	 * @param int $R red
	 * @param int $G green
	 * @param int $B blue
	 */
	function setBackgroundColor($R,$G,$B)
	{
		$this->backgroundColor['R'] = $R;
		$this->backgroundColor['V'] = $G;
		$this->backgroundColor['B'] = $B;
	}

	/**
	 * Sets the border color.
	 *
	 * @param int $R red
	 * @param int $G green
	 * @param int $B blue
	 */
	function setBorderColor($R,$G,$B)
	{
		$this->borderColor['R'] = $R;
		$this->borderColor['V'] = $G;
		$this->borderColor['B'] = $B;
	}

	/**
	 * Sets the border size.
	 *
	 * @param int $width border width in pixel
	 */
	function setBorderWidth($width)
	{
		$this->borderWidth = (int)$width;
	}

	/**
	 * Sets the text color.
	 *
	 * @param int $R red
	 * @param int $G green
	 * @param int $B blue
	 */
	function setTextColor($R,$G,$B)
	{
		$this->textColor['R'] = $R;
		$this->textColor['V'] = $G;
		$this->textColor['B'] = $B;
	}

	/**
	 * Sets the image width.
	 *
	 * @param int $width image width in pixel
	 */
	function setImageWidth($width)
	{
		$this->imageWidth = $width;
	}

	/**
	 * Sets the image height.
	 *
	 * @param int $height image height in pixel
	 */
	function setImageHeight($height)
	{
		$this->imageHeight = $height;
	}

	/**
	 * Sets the truetype font
	 *
	 * @param string $font path to the font
	 * @param int $size font size
	 */
	function setFont($font, $size)
	{
		if(!is_readable($font))
		{
			die(captcha_translate("The police cannot be found"));
		}
		$this->font = $font;
		$this->fontSize = $size;
	}

	/**
	 * Defines if a shadow should be applied
	 *
	 * @param int $x horizontal offset
	 * @param int $y vertical offset
	 */
	function setShadow($x=false,$y=false)
	{
		$this->shadow = true;
		if($x)
		{
			$this->shadowX = (int)$x;
		}
		if($y)
		{
			$this->shadowY = (int)$y;
		}
	}

	/**
	 * Sets the shadow color
	 *
	 * @param int $R red
	 * @param int $V green
	 * @param int $B blue
	 */
	function setShadowColor($R,$G,$B)
	{
		$this->shadow = true;
		$this->shadowColor['R'] = $R;
		$this->shadowColor['V'] = $G;
		$this->shadowColor['B'] = $B;
	}

	/**
	 * Defines if an background image should be displayed
	 *
	 * @param string $image		Path to image
	 */
	function setBackgroundImage($image)
	{
		if(is_readable($image))
		{
			$this->backgroundImage = $image;
		}
	}

	/**
	 * Sets the text angle.
	 *
	 * @param int $angle angle in degrees
	 */
	function setTextAngle($angle)
	{
		$this->textAngle = (int)$angle;
	}

	/**
	 * Sets the margin size relatively to the border
	 *
	 * @param int $margin margin size in pixels
	 */
	function setMarginFromBorder($margin)
	{
		$this->fromBorder = (int)$margin;
	}

	function setRoundedCorners($radius=false)
	{
		$this->roundedCorners = true;
		if($radius)
		{
			$this->roundedCornersRadius = (int)$radius;
		}
	}


	/**
	 * Build and return the image.
	 */
	function getImage()
	{
		if(!$this->font)
		{
			die(captcha_translate("The police must be loaded"));
		}

		$text = $this->getRandString();
		$text = trim(preg_replace('`(\w)`', '$1  ', $text));
		$box = imagettfbbox($this->fontSize,$this->textAngle,$this->font,$text);

		if(!$this->imageHeight)
		{
			$boxHeight = max($box[1],$box[3]) - min($box[7],$box[5]);
			$this->imageHeight = $boxHeight + $this->borderWidth*2 + $this->fromBorder*2;
		}
		if(!$this->imageWidth)
		{
			$boxWidth = max($box[4],$box[2]) - min($box[6],$box[0]);
			$this->imageWidth =  $boxWidth + $this->borderWidth*2 + $this->fromBorder*2;
		}

		if(function_exists('imagecreatetruecolor'))
		{
			$im = imagecreatetruecolor($this->imageWidth, $this->imageHeight);
		}
		else
		{
			$im = imagecreate($this->imageWidth, $this->imageHeight);
		}

		if($this->borderWidth > 0)
		{
			$border = imagecolorallocate(
				$im,
				$this->borderColor['R'],
				$this->borderColor['V'],
				$this->borderColor['B']
			);
			if(!$this->roundedCorners)
			{
				imagefilledrectangle(
					$im,
					0,
					0,
					$this->imageWidth,
					$this->imageHeight,
					$border
				);
			}
			else
			{
				$this->ImageRectangleWithRoundedCorners(
					$im,
					0,
					0,
					$this->imageWidth,
					$this->imageHeight,
					$border,
					$this->roundedCornersRadius
				);
			}
		}

		$background = imagecolorallocate(
			$im,
			$this->backgroundColor['R'],
			$this->backgroundColor['V'],
			$this->backgroundColor['B']
		);

		imagefilledrectangle(
			$im,
			$this->borderWidth,
			$this->borderWidth,
			$this->imageWidth-$this->borderWidth,
			$this->imageHeight-$this->borderWidth,
			$background
		);

		if($this->backgroundImage)
		{
			// Calcul des nouvelles dimensions
			list($width, $height,$type) = getimagesize($this->backgroundImage);

			$new_width = $this->imageWidth-$this->borderWidth*2;
			$new_height = $this->imageHeight-$this->borderWidth*2;

			if($type === 1)
			{
				$type_ = 'gif';
			}
			elseif($type === 2)
			{
				$type_ = 'jpeg';
			}
			elseif($type === 3)
			{
				$type_ = 'png';
			}
			else
			{
				die(captcha_translate("Wrong type for the background picture"));
			}
			$fct = 'imagecreatefrom' . $type_;
			$imb = $fct($this->backgroundImage);


			imagecopyresampled(
				$im,
				$imb,
				$this->borderWidth,
				$this->borderWidth,
				0,
				0,
				$new_width,
				$new_height,
				$width,
				$height
			);

			imagedestroy($imb);
		}

		$textColor = imagecolorallocate (
			$im,
			$this->textColor['R'],
			$this->textColor['V'],
			$this->textColor['B']
		);

		$x = ($this->imageWidth - $boxWidth)/2;

		$y = $this->imageHeight   - $this->borderWidth - $this->fromBorder;

		if($this->shadow)
		{
			$shadow = imagecolorallocate(
				$im,
				$this->shadowColor['R'],
				$this->shadowColor['V'],
				$this->shadowColor['B']
				);
				imagettftext(
				$im,
				$this->fontSize,
				$this->textAngle,
				$x+$this->shadowX,
				$y+$this->shadowY,
				$shadow,
				$this->font,
				$text
			);
		}

		imagettftext(
			$im,
			$this->fontSize,
			$this->textAngle,
			$x,
			$y,
			$textColor,
			$this->font,
			$text
		);

		$this->makeHeaders();
		$image_function = 'image' . $this->type;
		$image_function($im);
		imagedestroy($im);
	}
	
	
	function setRandString($str)
	{
		$this->randString = $str;
	}


	/**
	 * Returns a random string.
	 *
	 * @return string 	The random string.
	 */
	function getRandString()
	{
		if(!$this->randString)
		{
			$T = array(
				'A', 'B', 'D', 'E', 'F', 'G', 'H', 'M', 'N', 'Q', 'R', 'T', 'Y',
				'a', 'b', 'd', 'e', 'f', 'g', 'h', 'L', 'm', 'n', 'q', 'r', 't', 'y',
				'2', '3', '4', '5', '6', '7', '8', '9'
			);
			shuffle($T);
			$TT = array_chunk($T, $this->stringLength);

			$this->randString = implode('', $TT[0]);
		}
		return $this->randString;
	}

	function ImageRectangleWithRoundedCorners(&$im, $x1, $y1, $x2, $y2, $color, $radius)
	{
		// transparency
		$trans = imageColorAllocate ($im, 255, 255, 255);
		$color_ = imagecolortransparent($im, $trans);
		// cornerless rectangle
		imagefilledrectangle($im, $x1, $y1, $x2, $y2, $color_);
		imagefilledrectangle($im, $x1+$radius, $y1, $x2-$radius, $y2, $color);
		imagefilledrectangle($im, $x1, $y1+$radius, $x2, $y2-$radius, $color);
		// rounded corners
		imagefilledellipse($im, $x1+$radius, $y1+$radius, $radius*2, $radius*2, $color);
		imagefilledellipse($im, $x2-$radius, $y1+$radius, $radius*2, $radius*2, $color);
		imagefilledellipse($im, $x1+$radius, $y2-$radius, $radius*2, $radius*2, $color);
		imagefilledellipse($im, $x2-$radius, $y2-$radius, $radius*2, $radius*2, $color);

	}

	function setImageType($type)
	{
		switch(strtolower($type))
		{
			case 'gif' :
			case 'png' :
			case 'jpeg' :
				$this->type = $type;
				break;
			case 'jpg' :
				$this->type = 'jpeg';
				break;
			default :
				$this->type = 'png';
		}

		if(!function_exists('image'.$this->type))
		{
			die(captcha_translate("The function is not available") . ' ' . 'image'.$this->type);
		}
	}

	function makeHeaders()
	{
		header('Expires: Mon, 01 Jan 2000 00:00:00 GMT');
		header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', false);
		header('Pragma: no-cache');
		header('Content-Type: image/' . $this->type);
	}

	function getGetSecurityHtmlData()
	{
		$oInfo = bab_getAddonInfosInstance('captcha');

		if (!$oInfo || !$oInfo->isAccessValid()) {
			return bab_toHtml(captcha_translate('Error, captcha addon is not installed'));
		}

		return '<img class="captcha-image" src="'.bab_toHtml(bab_getSelf().'?addon=captcha.captchaIdx&sIdx=' . CPT_GET_IMAGE_IDX.'&ts='.number_format(microtime(true), 3,'.','')) .
			'" onclick="' . bab_toHtml('this.src=this.src.replace(/^(.*)&ts=([^&]*)$/, \'$1\') + \'&ts=\' + (new Date().getTime()/1000);') . '" />';
	}

	function securityCodeValid($sSecurityCode)
	{
		if(array_key_exists('sCaptchaSecurityCode', $_SESSION))
		{
			if(strlen(trim($sSecurityCode)) > 0 && strlen($_SESSION['sCaptchaSecurityCode']))
			{
				return ((string) $sSecurityCode === (string) $_SESSION['sCaptchaSecurityCode']);
			}
		}
		return false;
	}
}
