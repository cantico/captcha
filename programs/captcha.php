<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


function captcha_getImage()
{
	$oCaptcha = @bab_functionality::get('Captcha');
	/*@var $oCaptcha Func_Captcha */

	if(false !== $oCaptcha)
	{
		$ts = bab_gp('ts');
		if (isset($_SESSION['sCaptchaSecurityLastTime']) && $_SESSION['sCaptchaSecurityLastTime'] === $ts)
		{
			if (isset($_SESSION['sCaptchaSecurityCode']))
			{
				// a code is allready generated with the same URL
				$oCaptcha->setRandString($_SESSION['sCaptchaSecurityCode']);
			}
		}
		
		$_SESSION['sCaptchaSecurityLastTime'] = $ts;
		
		$oCaptcha->setStringLenght(4);
		
		$oCaptcha->setBorderColor(255,255,255);
		$oCaptcha->setBorderWidth(1);
		
		$oCaptcha->setTextAngle(rand(2,10));
		
		$oCaptcha->setShadow();
		
		$oCaptcha->setRoundedCorners(5);
		
		$oCaptcha->getImage();
		
		$_SESSION['sCaptchaSecurityCode'] = $oCaptcha->getRandString();
	}
	exit;
}


?>