<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * <OFCaptchaField class="">
 */
class Func_Ovml_Function_CaptchaField extends Func_Ovml_Function
{
    public function toString()
    {
        $captcha = bab_functionality::get('Captcha');
        /*@var $captcha Func_Captcha */
        
        if (!isset($this->args['type'])) {
            $this->args['type'] = 'text';
        }
        
        if (!isset($this->args['name'])) {
            $this->args['name'] = 'captchaSecurityCode';
        }
        
        // serialize unknown arguments to input attribute
        $ovmlAttr = array();
        $attributes = array();
        foreach ($this->args as $name => $value) {
            switch($name) {
                case 'saveas':
                    $ovmlAttr[$name] = $value;
                    break;
                default:
                    $attributes[] = bab_toHtml($name).'="'.bab_toHtml($value).'"';
            }
            
        }

        $image = $captcha->getGetSecurityHtmlData();
        $field = '<input '.implode(' ', $attributes).' />';

        return $this->format_output($image.$field, $ovmlAttr, bab_context::HTML);
    }
}