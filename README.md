## Gestion d'un captcha ##

Librairie permettant d'ajouter la gestion d'un captcha dans d'autres modules (test pour différencier de manière automatisée un utilisateur humain d'un ordinateur).

Avec ce module, la saisie de captcha sera également ajoutée sur certaines fonctionnalités d'Ovidentia comme les forums ou l'inscription.